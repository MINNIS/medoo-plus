# Medoo+

A wrapper to bring even more power to **catfan/medoo** 
> *the lightweight PHP database framework to accelerate development*
* Official Medoo website: [https://medoo.in](https://medoo.in)

## Install

The package can be installed using [composer](https://getcomposer.org):
> composer require minnis/medoo-plus


### Usage

This package extends catfan/medoo. 

It is a drop-in replacement, just change your **Medoo\Medoo** namespaces to **MINNIS\Medoo\Medoo**


## Medoo+

Improvements:

* (mysql/mariadb) Connection option *ATTR_EMULATE_PREPARES* is now default set to *false*. Integer columns will now return integers instead of strings.
* Mapping (data/column return types) of fetched data
* \DateTime as return type on date/datetime/timestamp columns
* Constants to map return types  
* Minor speed improvement
  

## MAPS! (return type mapping)
Medoo has return type support by adding its type to the column name, like: ```'is_active [Bool]'```.

In many cases this works fine but this method has a few drawbacks. These are solved by using a resultmap.

To use a resultmap add **MAP** _(all uppercase)_ to the **$where** parameter on the select() or get() method, just like _LIMIT_, _ORDER_, etc:
```php
$database->select('table', $columns, [
    'LIMIT' => 10,
    'MAP'   => [
        'column_name'  => Medoo::RETURN_BOOL,
        'other_column' => Medoo::RETURN_DATETIME,
    ],   
]);
```

**Benefits**

- Mapping on wildcard __*__ selects
- Clean column names in $columns arrays
- The contents of MAP array can by set by a single variable (easy re-usage)
- Constants for autocompletion in your IDE
- A new return type: DateTime object 


**DateTime**

Getting strings from a _Date_, _Datetime_ or _Timestamp_ column serve little purpose. 
In most cases you will propably use the value to create a **\DateTime** object.
 
Now you can simply map it as return type using **Medoo::RETURN_DATETIME**.

This return type can be used on _date_, _datetime_, _timestamp_ columns, 
as well on every column returning date[time] using _yyyy-mm-dd [hh:mm:ss]_ notation or a _numeric value_ (unix timestamp).

**false** _(boolean)_ is returned if the source value is: null, false, an empty string, 0, '0000-00-00', '0000-00-00 00:00:00' 
and when the  DateTime object could not be created due to a invalid value.  



## Example

``` php
<?php

use MINNIS\Medoo\Medoo;

require_once __DIR__ . '/../vendor/autoload.php';

$database = new Medoo(/* see medoo docs for details */);

// Default
$default_medoo = $database->select('users', [
    'username',
    'is_active',
    'signup_date',
], [
    'user_id' => 50,
]);

var_dump($default_medoo);

array(1) {
  [0] =>
  array(3) {
    'username' =>
    string(3) "foo"
    'is_active' =>
    string(1) "1"
    'signup_date' =>
    string(10) "2025-06-01"
  }
}

// Using MAP
$medoo_plus = $database->select('users', [
    'username',
    'is_active',
    'signup_date',
], [
    'user_id' => 50,
    'MAP'     => [
      'is_active'   => Medoo::RETURN_BOOL,
      'signup_date' => Medoo::RETURN_DATETIME,
    ]
]);

var_dump($medoo_plus);

array(1) {
  [0] =>
  array(3) {
    'username' =>
    string(3) "foo"
    'is_active' =>
    bool(true)
    'signup_date' =>
    class DateTime#1 (3) {
      public $date =>
      string(26) "2025-06-01 00:00:00.000000"
      public $timezone_type =>
      int(3)
      public $timezone =>
      string(16) "Europe/Amsterdam"
    }
  }
}
```

## License

This package is under the MIT license.


### Documentation
* Medoo Documentation: [https://medoo.in/doc](https://medoo.in/doc)
