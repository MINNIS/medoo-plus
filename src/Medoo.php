<?php

namespace MINNIS\Medoo;

use PDO;

class Raw extends \Medoo\Raw
{
    public $value;
    public $map;

    public function __construct($value, $map = [])
    {
        $this->value = $value;
        $this->map   = $map;
    }
}

class Medoo extends \Medoo\Medoo
{
    const RETURN_BOOL = 'Bool';
    const RETURN_DATETIME = 'DateTime';
    const RETURN_INT = 'Int';
    const RETURN_JSON = 'JSON';
    const RETURN_NUMBER = 'Number';
    const RETURN_OBJECT = 'Object';
    const RETURN_STRING = 'String';

    public $emulate_default = false;
    public $emulate_current = false;

    public function __construct(array $options)
    {
        if (isset($options['database_type'])) {

            $this->type = strtolower($options['database_type']);

            if ($this->type === 'mysql' || $this->type === 'mariadb') {

                if (!isset($options['option'][PDO::ATTR_DEFAULT_FETCH_MODE])) {

                    $options['option'][PDO::ATTR_DEFAULT_FETCH_MODE] = PDO::FETCH_ASSOC;
                }

                if (!isset($options['option'][PDO::ATTR_EMULATE_PREPARES])) {

                    $options['option'][PDO::ATTR_EMULATE_PREPARES] = false;
                }

                $this->emulate_default = $this->emulate_current = (bool)$options['option'][PDO::ATTR_EMULATE_PREPARES];
            }
        }

        parent::__construct($options);
    }


    protected function selectContext($table, &$map, $join, &$columns = null, $where = null, $column_fn = null)
    {
        preg_match('/(?<table>[a-zA-Z0-9_]+)\s*\((?<alias>[a-zA-Z0-9_]+)\)/i', $table, $table_match);

        if (isset($table_match[ 'table' ], $table_match[ 'alias' ]))
        {
            $table = $this->tableQuote($table_match[ 'table' ]);

            $table_query = $table . ' AS ' . $this->tableQuote($table_match[ 'alias' ]);
        }
        else
        {
            $table = $this->tableQuote($table);

            $table_query = $table;
        }

        $is_join = false;
        $join_key = is_array($join) ? array_keys($join) : null;

        if (
            isset($join_key[ 0 ]) &&
            strpos($join_key[ 0 ], '[') === 0
        )
        {
            $is_join = true;
            $table_query .= ' ' . $this->buildJoin($table, $join);
        }
        else
        {
            if (is_null($columns))
            {
                if (
                    !is_null($where) ||
                    (is_array($join) && isset($column_fn))
                )
                {
                    $where = $join;
                    $columns = null;
                }
                else
                {
                    $where = null;
                    $columns = $join;
                }
            }
            else
            {
                $where = $columns;
                $columns = $join;
            }
        }

        if (isset($column_fn))
        {
            if ($column_fn === 1)
            {
                $column = '1';

                if (is_null($where))
                {
                    $where = $columns;
                }
            }
            elseif ($raw = $this->buildRaw($column_fn, $map))
            {
                $column = $raw;
            }
            else
            {
                if (empty($columns) || $this->isRaw($columns))
                {
                    $columns = '*';
                    $where = $join;
                }

                $column = $column_fn . '(' . $this->columnPush($columns, $map, true) . ')';
            }
        }
        else
        {
            $column = $this->columnPush($columns, $map, true, $is_join);
        }

        // Move "MAP" from $where to $columns
        if (isset($where['MAP'])) {

            if ($columns === '*') {

                $columns = ['*'];
            }

            $columns['MAP'] = $where['MAP'];

            unset($where['MAP']);
        }

        return 'SELECT ' . $column . ' FROM ' . $table_query . $this->whereClause($where, $map);
    }

    protected function columnMap($columns, &$stack, $root)
    {
        $typemap = null;

        if (isset($columns['MAP'])) {

            $typemap = $columns['MAP'];

            unset($columns['MAP']);

            // If * is used, return MAP(stack)
            if (current($columns) === '*') {

                foreach ($typemap as $columnName => $returnDataType) {

                    $stack[$columnName] = [$columnName, $returnDataType];
                }

                return $stack;
            }
        }

        if ($columns === '*')
        {
            return $stack;
        }

        foreach ($columns as $key => $value)
        {
            if (is_int($key))
            {
                preg_match('/([a-zA-Z0-9_]+\.)?(?<column>[a-zA-Z0-9_]+)(?:\s*\((?<alias>[a-zA-Z0-9_]+)\))?(?:\s*\[(?<type>(?:String|Bool|Int|Number|Object|JSON))\])?/i', $value, $key_match);

                $column_key = !empty($key_match[ 'alias' ]) ?
                    $key_match[ 'alias' ] :
                    $key_match[ 'column' ];

                if (isset($key_match[ 'type' ]))
                {
                    $stack[ $value ] = [$column_key, $key_match[ 'type' ]];
                }
                else
                {
                    $stack[ $value ] = [$column_key, 'String'];
                }
            }
            elseif ($this->isRaw($value))
            {
                preg_match('/([a-zA-Z0-9_]+\.)?(?<column>[a-zA-Z0-9_]+)(\s*\[(?<type>(String|Bool|Int|Number))\])?/i', $key, $key_match);

                $column_key = $key_match[ 'column' ];

                if (isset($key_match[ 'type' ]))
                {
                    $stack[ $key ] = [$column_key, $key_match[ 'type' ]];
                }
                else
                {
                    $stack[ $key ] = [$column_key, 'String'];
                }
            }
            elseif (!is_int($key) && is_array($value))
            {
                if ($root && count(array_keys($columns)) === 1)
                {
                    $stack[ $key ] = [$key, 'String'];
                }

                $this->columnMap($value, $stack, false);
            }
        }

        // Overwrite MAP(stack) if set
        if (null !== $typemap) {

            foreach ($typemap as $columnName => $returnDataType) {

                $stack[$columnName][1] = $returnDataType;
            }
        }

        return $stack;
    }

    protected function dataMap($data, $columns, $column_map, &$stack, $root, &$result)
    {
        if ($root)
        {
            // Reset MAP (when provided)
            unset ($columns['MAP']);

            $columns_key = array_keys($columns);

            if (count($columns_key) === 1 && is_array($columns[$columns_key[0]]))
            {
                $index_key = array_keys($columns)[0];
                $data_key = preg_replace("/^[a-zA-Z0-9_]+\./i", "", $index_key);

                $current_stack = [];

                foreach ($data as $item)
                {
                    $this->dataMap($data, $columns[ $index_key ], $column_map, $current_stack, false, $result);

                    $index = $data[ $data_key ];

                    $result[ $index ] = $current_stack;
                }
            }
            else
            {
                $current_stack = [];

                $this->dataMap($data, $columns, $column_map, $current_stack, false, $result);

                $result[] = $current_stack;
            }

            return;
        }

        foreach ($columns as $key => $value)
        {
            $isRaw = $this->isRaw($value);

            if (is_int($key) || $isRaw)
            {
                $map = $column_map[ $isRaw ? $key : $value ];

                $column_key = $map[ 0 ];

                $item = $data[ $column_key ];

                if (isset($map[ 1 ]))
                {
                    if ($isRaw && in_array($map[ 1 ], [self::RETURN_OBJECT, self::RETURN_JSON], true))
                    {
                        continue;
                    }

                    if (null === $item)
                    {
                        $stack[ $column_key ] = null;
                        continue;
                    }

                    switch ($map[ 1 ])
                    {
                        case self::RETURN_STRING:
                            $stack[ $column_key ] = (string)$item;
                            break;

                        case self::RETURN_INT:
                            $stack[ $column_key ] = (int)$item;
                            break;

                        case self::RETURN_BOOL:
                            $stack[ $column_key ] = (bool)$item;
                            break;

                        case self::RETURN_DATETIME:
                            $stack[ $column_key ] = $this->toDateTime($item);
                            break;

                        case self::RETURN_JSON:
                            $stack[ $column_key ] = json_decode($item, true);
                            break;

                        case self::RETURN_OBJECT:
                            $stack[ $column_key ] = unserialize($item);
                            break;

                        case self::RETURN_NUMBER:
                            $stack[ $column_key ] = (double) $item;
                            break;
                    }
                }
                else
                {
                    $stack[ $column_key ] = $item;
                }
            }
            else
            {
                $current_stack = [];

                $this->dataMap($data, $value, $column_map, $current_stack, false, $result);

                $stack[ $key ] = $current_stack;
            }
        }
    }

    public function id()
    {
        if ($this->statement === null) {

            return null;
        }

        // MySQL on top
        if ($this->type === 'mysql') {

            $lastId = $this->pdo->lastInsertId();

            return empty($lastId) ? null : $lastId;
        }

        if ($this->type === 'oracle') {

            return 0;
        }

        if ($this->type === 'pgsql') {

            return $this->pdo->query('SELECT LASTVAL()')->fetchColumn();
        }

        $lastId = $this->pdo->lastInsertId();

        return empty($lastId) ? null : $lastId;
    }

    public function get($table, $join = null, $columns = null, $where = null)
    {
        $map = [];
        $result = [];
        $column_map = [];
        $current_stack = [];

        if ($where === null)
        {
            $column = $join;
            unset($columns[ 'LIMIT' ]);
        }
        else
        {
            $column = $columns;
            unset($where[ 'LIMIT' ]);
        }

        $is_single = (is_string($column) && $column !== '*');

        $query = $this->exec($this->selectContext($table, $map, $join, $columns, $where) . ' LIMIT 1', $map);

        if ($query)
        {
            $data = $query->fetchAll(PDO::FETCH_ASSOC);

            if (isset($data[ 0 ]))
            {
                if ($column === '*')
                {
                    return $this->wildcardMap($data[ 0 ], $columns);
                }

                $this->columnMap($columns, $column_map, true);

                $this->dataMap($data[ 0 ], $columns, $column_map, $current_stack, true, $result);

                if ($is_single)
                {
                    return $result[ 0 ][ $column_map[ $column ][ 0 ] ];
                }

                return $result[ 0 ];
            }
        }
    }

    public function select($table, $join, $columns = null, $where = null)
    {
        $map = [];
        $result = [];
        $column_map = [];

        $index = 0;

        $column = $where === null ? $join : $columns;

        $is_single = (is_string($column) && $column !== '*');

        $query = $this->exec($this->selectContext($table, $map, $join, $columns, $where), $map);

        $this->columnMap($columns, $column_map, true);

        if (!$this->statement)
        {
            return false;
        }

        if ($column === '*')
        {
            return $this->wildcardMap($query->fetchAll(PDO::FETCH_ASSOC), $columns);
        }

        while ($data = $query->fetch(PDO::FETCH_ASSOC))
        {
            $current_stack = [];

            $this->dataMap($data, $columns, $column_map, $current_stack, true, $result);
        }

        if ($is_single)
        {
            $single_result = [];
            $result_key = $column_map[ $column ][ 0 ];

            foreach ($result as $item)
            {
                $single_result[] = $item[ $result_key ];
            }

            return $single_result;
        }

        return $result;
    }

    private function wildcardMap($data, $columns)
    {
        if (false === isset($columns['MAP'])) {

            return $data;
        }

        foreach ($data as $key => &$value) {

            if (false === isset($columns['MAP'][$key])) {

                continue;
            }

            if (is_null($value)) {

                continue;
            }

            switch ($columns['MAP'][$key]) {

                case self::RETURN_STRING:
                    $value = (string)$value;
                    break;

                case self::RETURN_INT:
                    $value = (int)$value;
                    break;

                case self::RETURN_BOOL:
                    $value = (bool)$value;
                    break;

                case self::RETURN_DATETIME:
                    $value = $this->toDateTime($value);
                    break;

                case self::RETURN_JSON:
                    $value = json_decode($value, true);
                    break;

                case self::RETURN_NUMBER:
                    $value = (double)$value;
                    break;

                case self::RETURN_OBJECT:
                    $value = unserialize($value);
                    break;
            }
        }

        return $data;
    }


    private function toDateTime($item)
    {
        if (empty($item) || $item === '0000-00-00' || $item === '0000-00-00 00:00:00') {

            return false;
        }

        if (is_numeric($item)) {

            $item = '@' . $item;
        }

        try {
            return new \DateTime($item);

        } catch (\Exception $exception) {

            return false;
        }
    }

    protected function buildRaw($raw, &$map)
    {
        if (!$this->isRaw($raw))
        {
            return false;
        }

        $query = preg_replace_callback(
            '/(([`\']).*?)?((FROM|TABLE|INTO|UPDATE|JOIN)\s*)?\<(([a-zA-Z0-9_]+)(\.[a-zA-Z0-9_]+)?)\>(.*?\2)?/i',
            function ($matches)
            {
                if (!empty($matches[ 2 ]) && isset($matches[ 8 ]))
                {
                    return $matches[ 0 ];
                }

                if (!empty($matches[ 4 ]))
                {
                    return $matches[ 1 ] . $matches[ 4 ] . ' ' . $this->tableQuote($matches[ 5 ]);
                }

                return $matches[ 1 ] . $this->columnQuote($matches[ 5 ]);
            },
            $raw->value);

        $raw_map = $raw->map;

        if (!empty($raw_map))
        {
            foreach ($raw_map as $key => $value)
            {
                $map[ $key ] = $this->typeMap($value, gettype($value));
            }
        }

        $this->emulate_current = true;

        return $query;
    }

    public function exec($query, $map = [])
    {
        $this->statement = null;

        if ($this->debug_mode)
        {
            echo $this->generate($query, $map);

            $this->debug_mode = false;

            return false;
        }

        if ($this->logging)
        {
            $this->logs[] = [$query, $map];
        }
        else
        {
            $this->logs = [[$query, $map]];
        }

        if ($this->type === 'mysql' && $this->emulate_default !== $this->emulate_current) {
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, $this->emulate_current);
        }

        $statement = $this->pdo->prepare($query);

        if (!$statement)
        {
            $this->errorInfo = $this->pdo->errorInfo();
            $this->statement = null;

            return false;
        }

        $this->statement = $statement;
        foreach ($map as $key => $value)
        {
            $statement->bindValue($key, $value[ 0 ], $value[ 1 ]);
        }

        $execute = $statement->execute();

        $this->errorInfo = $statement->errorInfo();

        if (!$execute)
        {
            $this->statement = null;
        }

        if ($this->type === 'mysql') {
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, $this->emulate_default);
            $this->emulate_current = $this->emulate_default;
        }

        return $statement;
    }


    public static function NOW()
    {
        return new Raw('NOW()');
    }
}
